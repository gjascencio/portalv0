import { FeriasComponent } from '../ferias/ferias.component';
import { BancoDeHorasComponent } from '../banco-de-horas/banco-de-horas.component';
import { HomeComponent } from '../home/home.component';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { LoginComponent } from '../login/login.component';
import { TesteComponent } from '../teste/teste.component';
export const ROUTES = [
    {
        path: '',
        component: HomeComponent
    },
    {
        path: 'controle/ferias',
        component: FeriasComponent
    },
    {
        path: 'controle/bancoHoras',
        component: BancoDeHorasComponent
    },
    {
        path: 'dashboard',
        component: DashboardComponent
    }, 
    {
        path: 'auth/login',
        component: LoginComponent
    },
    {
        path: 'teste',
        component: TesteComponent
    }
]