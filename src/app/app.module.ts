import { NgModule } from '@angular/core'
import { ROUTES } from './routes/routes'
import { HttpModule } from '@angular/http'
import { RouterModule } from '@angular/router'
import { AppComponent } from './app.component'
import { LoginComponent } from './login/login.component'
import { BrowserModule } from '@angular/platform-browser'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { HomeComponent } from './home/home.component'
import { FeriasComponent } from './ferias/ferias.component'
import { BancoDeHorasComponent } from './banco-de-horas/banco-de-horas.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { TesteComponent } from './teste/teste.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    FeriasComponent,
    BancoDeHorasComponent,
    DashboardComponent,
    TesteComponent
  ],
  imports: [
    BrowserModule, 
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    RouterModule.forRoot(ROUTES),
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
