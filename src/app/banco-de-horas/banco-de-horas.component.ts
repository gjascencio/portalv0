import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-banco-de-horas',
  templateUrl: './banco-de-horas.component.html',
  styleUrls: ['./banco-de-horas.component.css']
})
export class BancoDeHorasComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  private fieldArray: Array<any> = [];
  private newAttribute: any = {};

  addFieldValue() {
    this.fieldArray.push(this.newAttribute)
    this.newAttribute = {};
  }

  deleteFieldValue(index) {
    this.fieldArray.splice(index, 1);
  }

}
